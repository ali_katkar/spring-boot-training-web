package com.akatkar.training.spring.web;

import java.util.*;
import java.util.stream.Collectors;

public class BookingRepository {

	private Integer nextId = 1;
	private Map<Integer, HotelBooking> bookings = new HashMap<>();
	
	
	public List<HotelBooking> findAll() {
		return this.bookings.values().stream()
				.collect(Collectors.toList());
	}

	public HotelBooking findById(int id) {
		return bookings.get(id);
	}

	public HotelBooking save(HotelBooking booking) {
		booking.setId(nextId);
		bookings.put(nextId, booking);
		nextId ++;
		return booking;
	}

	public List<HotelBooking> save(List<HotelBooking> bookings) {
		bookings.forEach(this::save);
		return findAll();
	}
	
	public void delete(int id) {
		bookings.remove(id);
	}
	
	public void deleteAll() {
		bookings.clear();
	}	
}
