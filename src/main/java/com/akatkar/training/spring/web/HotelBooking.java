package com.akatkar.training.spring.web;

public class HotelBooking {
	
	private Integer id;
	private String hotelName;
	private Double pricePerNight;
	private int nbOfNights;

	public HotelBooking() {
	}

	public HotelBooking(String hotelName, double pricePerNight, int nbOfNights) {
		super();
		this.hotelName = hotelName;
		this.pricePerNight = pricePerNight;
		this.nbOfNights = nbOfNights;
	}

	// Getter and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public Double getPricePerNight() {
		return pricePerNight;
	}

	public void setPricePerNight(Double pricePerNight) {
		this.pricePerNight = pricePerNight;
	}

	public int getNbOfNights() {
		return nbOfNights;
	}

	public void setNbOfNights(int nbOfNights) {
		this.nbOfNights = nbOfNights;
	}

	@Override
	public String toString() {
		return "HotelBooking [id=" + id + ", hotelName=" + hotelName + ", pricePerNight=" + pricePerNight
				+ ", nbOfNights=" + nbOfNights + "]";
	}

	
}
