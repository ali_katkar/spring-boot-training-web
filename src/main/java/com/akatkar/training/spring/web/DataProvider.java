package com.akatkar.training.spring.web;

import java.util.ArrayList;
import java.util.List;

public class DataProvider {

	private final BookingRepository bookingRepository = new BookingRepository();
	
	public void createHotelBookings() {
		List<HotelBooking> bookings = new ArrayList<>();
		bookings.add(new HotelBooking("dedeman", 100.0, 1));
		bookings.add(new HotelBooking("shreton", 200.0, 1));
		bookings.add(new HotelBooking("hilton", 300.0, 1));
		bookings.add(new HotelBooking("divan", 400.0, 1));
		bookings.add(new HotelBooking("black mont", 500.0, 1));	
		bookingRepository.save(bookings);
	}
	
	public static void main(String[] args) {
		DataProvider dataProvider = new DataProvider();
		dataProvider.createHotelBookings();
		
		dataProvider.bookingRepository.findAll().forEach(System.out::println);
	}
}
